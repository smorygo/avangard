<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Order;
use App\Partner;
use Validator;
class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 
	private $statuses = [
		'0'=>'новый',
		'10'=>'подтвержден',
		'20'=>'завершен',
	]; 
	
	private $validatorRules = array(
        'client_email' => 'required|string|email',
		'partner_id' => 'required',
		'status' => 'required',
    );
	
    public function index(Request $request)
    {
        //
		
		$current_type = ($request->has('type'))? $request->type : 'current' ;
		
		$order_types = [
			'expired'=>['title'=>'Просроченные',
				'condition'=>[
					'where'=>'orders.status=10 AND orders.delivery_dt < now()',
					'sort_f'=>'orders.delivery_dt',
					'sort_t'=>'desc',
					'limit'=>50
				],
			],
			'current'=>['title'=>'Текущие',
				'condition'=>[
					'where'=>'orders.status=10 AND orders.delivery_dt>=now() AND ADDDATE(now(), interval 24 hour)>=orders.delivery_dt',
					'sort_f'=>'orders.delivery_dt',
					'sort_t'=>'asc',
					'limit'=>0
				],
			],
			'new'=>['title'=>'Новые', 
				'condition'=>[
					'where'=>'orders.status=0 AND orders.delivery_dt>=now()',
					'limit'=>50,
					'sort_f'=>'orders.delivery_dt',
					'sort_t'=>'asc',
				],
			],	
			'done'=>['title'=>'Выполненные',
				'condition'=>[
					'where'=>'orders.status=20 AND DATE_FORMAT(orders.delivery_dt,"%Y-%m-%d")=curdate()',
					'limit'=>50,
					'sort_f'=>'orders.delivery_dt',
					'sort_t'=>'desc',
				],
			],
		];
		$where = $order_types[$current_type]["condition"]["where"];
		$sort_f = $order_types[$current_type]["condition"]["sort_f"];
		$sort_t = $order_types[$current_type]["condition"]["sort_t"];
		$limit = $order_types[$current_type]["condition"]["limit"];
		$orders = Order::with(['partner','content.product','productSum'])->whereRaw($where)->orderBy($sort_f, $sort_t);
		if ($limit > 0){
			$orders = $orders->limit($limit);
		}
		$orders = $orders->get();
		//$orders = $orders->paginate(50);

		return view('orders.index', [ 'orders' => $orders,  'statuses'=>$this->statuses, 'order_types'=>$order_types, 'current_type'=>$current_type]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
		$order = Order::find($id);
		$partners = Partner::all();
		return view('orders.show', [ 'order' => $order,  'statuses'=>$this->statuses, 'partners'=>$partners]);
		
		
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
		
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
		//dd($id);
		 $validator = Validator::make($request->all(), $this->validatorRules);
		 //dd($validator->fails());
		 if ($validator->fails())
                return redirect()->route('orders.show',['id'=>$id])
                    ->withErrors($validator)
                    ->withInput($request->input());
		 
		 $order_data = [
			'status'=>$request->input('status'),
			'client_email'=>$request->input('client_email'),
			'partner_id'=>$request->input('partner_id'),
		 ];
		 
		 $order = Order::with(['partner','content.product','productSum'])->where('id',$id)->first();
		 //dd($order->content);
		 if ($request->input('status') == 20){
			$subject = "Заказ №".$id." завершен"; 
			$message = "";
			$a = 0;
			foreach ($order->content as $content){
				//dd($content);
				$a++;
				$message.=$a.". ".$content->product->name." - ".$content->quantity." шт, цена ".$content->price." руб.\n";
			}
			$message.="Общая стоимость заказа: ".$order->productSum[0]->total;
			//dd($message);
			
			
			$emails = (new Order)->getEmailList($id);
			foreach ($emails as $email){
				mail($email, $subject, $message);
			}
			//dd($emails); 
		 }
		 
		 
		 $order->update($order_data);
		 
		 return redirect()->route('orders.show',['id'=>$id])
				 ->with('success', 'Данные обновлены');
		 //dd($validator);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
