<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
	
	    protected $fillable = array
    (
		'status',
		'client_email',
		'partner_id'
    );
	
	public $statuses = [
		'0'=>'новый',
		'10'=>'подтвержден',
		'20'=>'завершен',
	];
	
	public function getEmailList($id){
		$emails = [];
		$order = Order::where('id',$id)->with(['partner','content.product.vendor'])->first();
		if (!is_null($order)){
			$emails[] = $order->partner->email;
			foreach ($order->content as $content){
				$emails[] = $content->product->vendor->email;
			}
		}
		return $emails;
	}
	
	public function partner()
	  {
		return $this->belongsTo('App\Partner');
	  }
	  
	public function content()
	  {
		return $this->hasMany('App\OrderProduct','order_id','id');
	  } 
	public function productSum()
	  {
		return $this->hasMany('App\OrderProduct','order_id','id')->selectRaw('order_products.order_id,SUM(order_products.quantity*order_products.price) as total') ->groupBy('order_products.order_id');
	  }  	
}
