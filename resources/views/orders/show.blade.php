<h1>Просмотр заказа</h1>
@if(is_null($order))
	Заказ не найден
@else
	@if (session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div><br><br> 
	@elseif (session()->has('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div><br><br>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="error-list">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br><br>
                @endif	

	<form method="POST" action="{{ route('orders.update', $order->id) }}" enctype="multipart/form-data">
        {{ csrf_field() }}
		{{ method_field('PUT') }}
		email клиента: <input type="email" value="{{ old('client_email', $order->client_email) }}" name="client_email"><br><br>
		партнер: <select name="partner_id"><option value=""></option>
		@foreach ($partners as $partner)
			<option value="{{ $partner->id }}" @if($partner->id == $order->partner_id) selected @endif>{{ $partner->name }}</option>
		@endforeach
		</select><br><br>
		статус заказа: <select name="status"><option value="">
		@foreach ($statuses as $k=>$v)
			<option value="{{ $k }}" @if($k == $order->status) selected @endif>{{ $v }}</option>
		@endforeach
		</option></select><br><br>
		
		@foreach($order->content as $content)
				{{ $content->product->name }} - {{ $content->quantity }} шт - {{ $content->price }} руб<br>
				@endforeach
		<br>
		стоимость заказ: {{ $order->productSum[0]->total }}
		<br><br>
		<input type="submit" value="Обновить">
	</form>
@endif	