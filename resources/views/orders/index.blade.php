<h1>Заказы</h1>

<div style="font-size: 20px; margin-bottom: 15px;">
@foreach($order_types as $type=>$type_data)
@if($type == $current_type)
	<b>{{ $type_data["title"] }}</b>
@else
	<a href="?type={{ $type }}">{{ $type_data["title"] }}</a>
@endif
@endforeach
</div>

<table cellspacing="0" cellpadding="3" border="1">
	<tr>
		<th>#</th>
		<th>ID заказа</th>
		<th>Название партнера</th>
		<th>Стоимость заказа</th>
		<th>Состав заказа</th>
		<th>Статус заказа</th>
		<th>Дата доставки</th>
	</tr>
	@foreach ($orders as $order)
		<tr>
			<th>{{ $loop->iteration }}</th>
			<td><a target="_blank" href="{{ route('orders.show',['id'=>$order->id]) }}">{{ $order->id }}</a></td>
			<td>{{ $order->partner->name }}</td>
			<td>{{ $order->productSum[0]->total }}</td>
			<td>
				@foreach($order->content as $content)
				{{ $content->product->name }}<br>
				@endforeach
			</td>
			<td>{{ $statuses[$order->status] }}</td>
			<td>{{ $order->delivery_dt }}</td>
		</tr>
	@endforeach
</table>